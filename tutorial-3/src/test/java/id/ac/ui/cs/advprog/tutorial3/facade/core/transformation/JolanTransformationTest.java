package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class JolanTransformationTest {
    private Class<?> jolanClass;

    @BeforeEach
    public void setup() throws Exception {
        jolanClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.JolanTransformation");
    }

    @Test
    public void testJolanHasEncodeMethod() throws Exception {
        Method translate = jolanClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testJolanEncodesCorrectly() throws Exception {
        String text = "FRYNZNgxgRfgxOReUNfVY";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Selamat test berhasil";

        Spell result = new JolanTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testJolanEncodesCorrectlyWithCustomShift() throws Exception {
        String text = "NZgVhVo5oZno5WZmcVndg";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Selamat test berhasil";

        Spell result = new JolanTransformation(5).encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testJolanHasDecodeMethod() throws Exception {
        Method translate = jolanClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testJolanDecodesCorrectly() throws Exception {
        String text = "Selamat test berhasil";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "FRYNZNgxgRfgxOReUNfVY";

        Spell result = new JolanTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testJolanDecodesCorrectlyWithCustomShift() throws Exception {
        String text = "Selamat test berhasil";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "NZgVhVo5oZno5WZmcVndg";

        Spell result = new JolanTransformation(5).decode(spell);
        assertEquals(expected, result.getText());
    }
}
