package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TransformationTest {
    private Class<?> transformationClass;

    @BeforeEach
    public void setup() throws Exception {
        transformationClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.Transformation");
    }

    @Test
    public void testTransformationHasEncodeMethod() throws Exception {
        Method translate = transformationClass.getDeclaredMethod("encode", String.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testTransformationHasDecodeMethod() throws Exception {
        Method translate = transformationClass.getDeclaredMethod("decode", String.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testTransformationDecodeCorrectlyImplemented() {
        String result = Transformation.decode("_M#!;JJB!Gx{X)b==?<D(&qDD_denCKVFD)Z%%w|@qnDBNF;NS%/D");
        assertEquals("FNSVeNxNaQx6xjRagxgbxNxOYNPXfZVgUxgbxSbeTRxbhexfjbeQm", result);
    }

    @Test
    public void testFacadeServiceEncodeCorrectlyImplemented() {
        String result = Transformation.encode("Safira and I went to a blacksmith to forge our sword");
        assertEquals("(/^:]ZZ$]{B=![AJJou(?<z((/HcS@X#_([),,xw|zS($%_r%*,p", result);
    }
}