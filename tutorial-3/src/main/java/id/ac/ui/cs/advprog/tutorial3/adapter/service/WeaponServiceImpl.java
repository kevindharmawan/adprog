package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

// TODO: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private BowRepository bowRepository;

    @Autowired
    private SpellbookRepository spellbookRepository;

    @Autowired
    private WeaponRepository weaponRepository;

    // TODO: implement me
    // DONE
    @Override
    public List<Weapon> findAll() {
        List<Weapon> list = weaponRepository.findAll();

        for (Bow bow : bowRepository.findAll()) {
            if (weaponRepository.findByAlias(bow.getName()) == null)
                list.add(new BowAdapter(bow));
        }
        for (Spellbook spellbook : spellbookRepository.findAll()) {
            if (weaponRepository.findByAlias(spellbook.getName()) == null)
                list.add(new SpellbookAdapter(spellbook));
        }

        return list;
    }

    // TODO: implement me
    // DONE
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon = weaponRepository.findByAlias(weaponName);

        if (weapon == null) {
            weapon = (bowRepository.findByAlias(weaponName) != null) ? new BowAdapter(bowRepository.findByAlias(weaponName)) : null;
        }
        if (weapon == null) {
            weapon = new SpellbookAdapter(spellbookRepository.findByAlias(weaponName));
        }

        logRepository.addLog(String.format(
                "%s attacked with %s (%s attack): %s",
                weapon.getHolderName(),
                weapon.getName(),
                attackType == 0 ? "normal" : "charged",
                attackType == 0 ? weapon.normalAttack() : weapon.chargedAttack()
        ));

        weaponRepository.save(weapon);
    }

    // TODO: implement me
    // DONE
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
