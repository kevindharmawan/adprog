package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
// DONE
public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private Boolean charged;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        this.charged = true;
    }

    @Override
    public String normalAttack() {
        charged = true;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        charged = !charged;
        return charged ? "Magic power not enough for large spell" : spellbook.largeSpell();
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
