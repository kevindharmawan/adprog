package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
/**
 * Kelas ini mengimplementasikan caesar cipher
 */
public class JolanTransformation {
    private int shift;

    public JolanTransformation(int shift){
        this.shift = shift;
    }

    public JolanTransformation(){
        this(13);
    }

    public Spell encode(Spell spell){
        return process(spell, true);
    }

    public Spell decode(Spell spell){
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode){
        int selector = encode ? 1 : -1;
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int codexSize = codex.getCharSize();
        char[] res = new char[text.length()];
        for(int i = 0; i < res.length; i++){
            int newCharIdx = codex.getIndex(text.charAt(i)) + shift * selector;
            if (newCharIdx < 0){
                newCharIdx += codexSize;
            }
            else if (newCharIdx >= codexSize){
                newCharIdx -= codexSize;
            }
            res[i] = codex.getChar(newCharIdx);
        }

        return new Spell(new String(res), codex);
    }
}
