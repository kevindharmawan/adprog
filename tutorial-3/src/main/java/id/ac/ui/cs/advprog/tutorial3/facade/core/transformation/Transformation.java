package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class Transformation {
    private static CelestialTransformation celestialTransformation = new CelestialTransformation();
    private static AbyssalTransformation abyssalTransformation = new AbyssalTransformation();
    private static JolanTransformation jolanTransformation = new JolanTransformation();

    public static String encode(String text) {
        Spell spell = new Spell(text, AlphaCodex.getInstance());
        spell = celestialTransformation.encode(spell);
        spell = abyssalTransformation.encode(spell);
        spell = jolanTransformation.encode(spell);
        spell = CodexTranslator.translate(spell, RunicCodex.getInstance());
        return spell.getText();
    }

    public static String decode(String code) {
        Spell spell = new Spell(code, RunicCodex.getInstance());
        spell = jolanTransformation.decode(spell);
        spell = abyssalTransformation.decode(spell);
        spell = celestialTransformation.decode(spell);
        spell = CodexTranslator.translate(spell, AlphaCodex.getInstance());
        return spell.getText();
    }
}
